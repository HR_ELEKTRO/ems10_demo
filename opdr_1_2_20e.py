def do_twice(f, v):
    f(v)
    f(v)
    
def line(width):
    print('-' * width)
    
do_twice(line, 40)
