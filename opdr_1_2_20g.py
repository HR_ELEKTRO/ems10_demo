#deze functie roept functie f twee keer aan
#met waarde v als argument
def do_twice(f, v):
    f(v)
    f(v)

def do_four(f, v):
    do_twice(f, v)
    do_twice(f, v)

def line(width):
    print('-' * width)
    
do_four(line, 10)